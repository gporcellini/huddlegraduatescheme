package navigator;

import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.List;

public class Nav {
	private List<Node> nodes;
	private MyDijkstra shortestPath; // used to compute shortest paths
	private MyRoutes otherRoutes; // used to visit the graph and find all path with a cost/edge condition
	
	/**
	 * empty constructor
	 */
	public Nav(){
		nodes = new ArrayList<Node>();
		shortestPath = new MyDijkstra();
		otherRoutes = new MyRoutes();
	}
	
	/**
	 * Adds a path from a node of ones currently present in the collection to another
	 * @param start: the beginning node of the path
	 * @param end: the ending node of the path
	 * @param cost: the cost of the path (>0)
	 * @throws InvalidParameterException: in case either start or end are 
	 * not part of the nodes of the nav or cost is negative
	 */
	public void addPath(String start, String end, int cost) throws InvalidParameterException{
		if (start == end || cost <= 0)
			throw new InvalidParameterException();
	
		Node startNode = Node.getNode(nodes, start);
		Node endNode = Node.getNode(nodes, end);
		if(startNode == null || endNode == null)
			throw new InvalidParameterException();
		startNode.addPath(endNode, cost);
	}
	
	/**
	 * Adds nodes to the ones of the nav with the specified names
	 * @param names: array containing the names of the nodes
	 */
	public void addNodes(String[] names){
		for (String name : names)
			addNode(name);
	}
	
	/**
	 * Adds a node to the ones of the nav with the specified name
	 * @param name
	 */
	public void addNode(String name) {
		this.nodes.add(new Node(name));
	}
	
	/**
	 * Gets the shortest path from start to end using Dijkstra
	 * @param start
	 * @param end
	 * @return a string containing names of traversed node during the shortest path separated by '-'
	 * @throws Exception
	 */
	public String getShortestPath(String start, String end) throws Exception {
		Node startNode = Node.getNode(nodes, start);
		Node endNode = Node.getNode(nodes, end);
		if (startNode == null || endNode == null)
			throw new InvalidParameterException();
		shortestPath.execute(startNode, endNode);
		StringBuilder sb = new StringBuilder();
		for (Node node : shortestPath.getPath())
			sb.append(node.name + "-");
		sb.setLength(sb.length() - 1);
		return sb.toString();
	}
	
	/**
	 * Gets the cost of the shortest path from start to end
	 * @param start
	 * @param end
	 * @return
	 * @throws Exception
	 */
	public int getShortestPathCost(String start, String end) throws Exception{
		Node startNode = Node.getNode(nodes, start);
		Node endNode = Node.getNode(nodes, end);
		if (startNode == null || endNode == null)
			throw new InvalidParameterException();
		Node.resetVisitedFlags(nodes);
		shortestPath.execute(startNode, endNode);
		return shortestPath.getCost();
	}
	
	/**
	 * Gets the cost of route passed as string containing nodes separated by '-'
	 * @param route
	 * @return
	 * @throws Exception
	 */
	public int getRouteCost(String route) throws Exception{
		if (route == null)
			throw new InvalidParameterException();
		List<Node> routeList = new ArrayList<Node>();
		String[] parts = route.split("-");
		for (String p : parts)
			routeList.add(Node.getNode(nodes, p));
		
		return otherRoutes.getRouteCost(routeList);
	}
	
	/**
	 * Gets the routes with N juctions from start to end
	 * @param start
	 * @param end
	 * @param n: the number of junctions
	 * @param max: can be excuted for maximum n junctions or exactly n
	 * @return
	 * @throws Exception
	 */
	public int getRoutesWithNJunctions(String start, String end, int n, boolean max) throws Exception{
		Node startNode = Node.getNode(nodes, start);
		Node endNode = Node.getNode(nodes, end);
		if (n <= 0 || startNode == null || endNode == null)
			throw new InvalidParameterException();

		return otherRoutes.getNEdgeRoutes(startNode, endNode, n, max);
	}
	
	/**
	 * Gets the routes with a cost lower than cost from start to end
	 * @param start
	 * @param end
	 * @param cost: the maximum cost
	 * @return
	 * @throws Exception
	 */
	public int getLessThanXCostRoutes(String start, String end, int cost) throws Exception{
		Node startNode = Node.getNode(nodes, start);
		Node endNode = Node.getNode(nodes, end);
		if (cost <= 0 || startNode == null || endNode == null)
			throw new InvalidParameterException();

		return otherRoutes.getXCostRoutes(startNode, endNode, cost-1 /*less than*/, true);
	}
}

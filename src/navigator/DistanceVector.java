package navigator;

import java.util.HashMap;
import java.util.Map;

public class DistanceVector<T> {
	private Map<T, Integer> distance;
	
	public DistanceVector(){
		distance = new HashMap<T, Integer>();
	}
	
	public void put(T key, int value){
		distance.put(key, value);
	}
	
	public int getValue(T key) {
	    Integer d = distance.get(key);
	    if (d == null) {
	      return Integer.MAX_VALUE;
	    } else {
	      return d;
	    }
	}
}

package navigator;

import java.util.List;

public class MyRoutes {
	public interface DecStrategy{
		public int op(int op, Path path);
	}
	
	public int found = 0;
	
	public int getRouteCost(List<Node> path) throws Exception{
		int cost = 0;
		for(int i = 0; i < path.size() -1; i++){
			Node start = path.get(i);
			Node next = path.get(i + 1);
			if (start != next){
				Path tmp = start.getPath(next);
				if (tmp == null)
					throw new Exception("NO SUCH ROUTE");
				
				cost += tmp.cost;
			}
		}
		return cost;
	}
	
	public int getNEdgeRoutes(Node start, Node end, int cost, boolean max){
		found = 0;
		for(Path path : start.getPaths())
			getNRoutesInner(path, end, cost, max, "" + start.name, (op, p) -> { return op-1; });
		return found;
	}
	
	public int getXCostRoutes(Node start, Node end, int cost, boolean max){
		found = 0;
		for(Path path : start.getPaths())
			getNRoutesInner(path, end, cost, max, "" + start.name, (op, p) -> { return op-p.cost; });
		return found;
	}
	
	private void getNRoutesInner(Path path, Node end, int cost, boolean max, String pathBuffer, DecStrategy decStrategy) {
		cost = decStrategy.op(cost, path);
		if (cost < 0)
			return;
		if (path.end == end){
			if (max || cost == 0){
				found++;
				//System.out.println("debug: " + pathBuffer + '-' + path.end.name);
			}
		}
		
		if (cost <= 0)
			return;
		for(Path next : path.end.getPaths())
			getNRoutesInner(next, end, cost, max, pathBuffer + '-' + path.end.name, decStrategy);
	}
}

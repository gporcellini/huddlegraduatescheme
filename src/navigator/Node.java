package navigator;

import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Node {
	public String name;
	public boolean visited;
	private List<Path> paths;
	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Node other = (Node) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	public static void resetVisitedFlags(List<Node> nodes){
		nodes.iterator().forEachRemaining((e) -> e.visited = false);
	}
	
	public static Node getNode(List<Node> nodes, String name){
		int tmp = nodes.indexOf(new Node(name));
		if (tmp != -1)
			return nodes.get(tmp);
		else return null;
	}
	
	public Node getClone(){
		Node tmp = new Node(name + ".1");
		tmp.paths = new ArrayList<Path>(paths);
		return tmp;
	}
	
	public Node(String name){
		this.name = name;
		this.visited = false;
		this.paths = new ArrayList<Path>();
	}
	
	public void addPath(Node end, int cost) throws InvalidParameterException{
		if (cost <= 0)
			throw new InvalidParameterException();
		this.paths.add(new Path(this, end, cost));
	}
	
	public Path getPath(Node neighbour){
		for (Path path : paths)
			if (path.end == neighbour)
				return path;
		return null;
	}
	
	public List<Path> getPaths(boolean excludeVisited) {
		List<Path> neighbours = new ArrayList<Path>();
		for (Path path : paths) {
			if (!excludeVisited || !path.end.visited) {
				neighbours.add(path);
			}
		}
		return neighbours;
	}
	
	public List<Path> getPaths() {
		return Collections.unmodifiableList(this.paths);
	}
}

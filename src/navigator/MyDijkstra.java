package navigator;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.PriorityQueue;

public class MyDijkstra {
	private PriorityQueue<Node> q;
	private Map<Node, Node> pred;
	private DistanceVector<Node> dist;
	Node start;
	Node end;

	public MyDijkstra() {
	}
	
	public LinkedList<Node> getPath() throws Exception {
		LinkedList<Node> path = new LinkedList<Node>();
		Node prev = end;
		if (pred.get(prev) == null)
			throw new Exception("NO PATH FOUND");
		path.add(prev);
		while (pred.get(prev) != null) {
			prev = pred.get(prev);
			path.add(prev);
		}
		
		Collections.reverse(path);
		return path;
    }
	
	public int getCost() throws Exception {
		return dist.getValue(end);
    }

	public void execute(Node s, Node e) {
		this.start = s != e ? s : s.getClone();
		this.end = e;
		q = new PriorityQueue<Node>(start.getPaths().size(), new Comparator<Node>(){
			public int compare(Node n1, Node n2) {
				return dist.getValue(n1) - dist.getValue(n2);
			}
	    }); // set of all the nodes not traversed yet
	    dist = new DistanceVector<Node>(); // maps nodes and distances from source
	    pred = new HashMap<Node, Node>(); // maps each node with its predecessors in the path
	    dist.put(this.start, 0);
	    q.add(this.start);
	    
	    while (q.size() > 0) {
	      Node next = q.poll();
	      if (end != null && next == this.end) // we can stop as we have found the minimum path to destination
	    	  return;
	      next.visited = true;

	      relax(next);
	    }
	}

	private void relax(Node current) {
		for (Path path : current.getPaths(true)) {
			if (dist.getValue(path.end) > dist.getValue(current) + path.cost) {
				// new minimum path
				dist.put(path.end, dist.getValue(current) + path.cost);
				pred.put(path.end, current);
				q.add(path.end);
			}
		}
	}
	

} 

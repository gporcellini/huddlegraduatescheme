import navigator.Nav;

public class HuddleGraduateScheme {

	public static void main(String[] args) {
		Nav myNav = new Nav();
		myNav.addNodes(new String[] { "A", "B", "C", "D", "E" } );
		myNav.addPath("A", "B", 5);
		myNav.addPath("B", "C", 4);
		myNav.addPath("C", "D", 7);
		myNav.addPath("D", "C", 8);
		myNav.addPath("D", "E", 6);
		myNav.addPath("A", "D", 5);
		myNav.addPath("C", "E", 2);
		myNav.addPath("E", "B", 3);
		myNav.addPath("A", "E", 7);
		
		//test case 1
		try {
			System.out.println("Test case 1: " + myNav.getRouteCost("A-B-C"));
		} catch (Exception e) {
			System.out.println("Test case 1: " + e.getMessage());
		}
		//test case 2
		try {
			System.out.println("Test case 2: " + myNav.getRouteCost("A-D"));
		} catch (Exception e) {
			System.out.println("Test case 2: " + e.getMessage());
		}
		//test case 3
		try {
			System.out.println("Test case 3: " + myNav.getRouteCost("A-D-C"));
		} catch (Exception e) {
			System.out.println("Test case 3: " + e.getMessage());
		}
		//test case 4
		try {
			System.out.println("Test case 4: " + myNav.getRouteCost("A-E-B-C-D"));
		} catch (Exception e) {
			System.out.println("Test case 4: " + e.getMessage());
		}
		//test case 5
		try {
			System.out.println("Test case 5: " + myNav.getRouteCost("A-E-D"));
		} catch (Exception e) {
			System.out.println("Test case 5: " + e.getMessage());
		}
		//test case 6
		try {
			System.out.println("Test case 6: " + myNav.getRoutesWithNJunctions("C", "C", 3, true));
		} catch (Exception e) {
			System.out.println("Test case 6: " + e.getMessage());
		}
		//test case 7
		try {
			System.out.println("Test case 7: " + myNav.getRoutesWithNJunctions("A", "C", 4, false));
		} catch (Exception e) {
			System.out.println("Test case 7: " + e.getMessage());
		}
		//test case 8
		try {
			//System.out.println("Test case 8: " + myNav.getShortestPath("A", "C"));
			System.out.println("Test case 8: " + myNav.getShortestPathCost("A", "C"));
		} catch (Exception e) {
			System.out.println("Test case 8: " + e.getMessage());
		}
		//test case 9
		try {
			//System.out.println("Test case 9: " + myNav.getShortestPath("B", "B"));
			System.out.println("Test case 9: " + myNav.getShortestPathCost("B", "B"));
		} catch (Exception e) {
			System.out.println("Test case 9: " + e.getMessage());
		}
		//test case 10
		try {
			//System.out.println("Test case 10: " + myNav.getShortestPath("B", "B"));
			System.out.println("Test case 10: " + myNav.getLessThanXCostRoutes("C", "C", 30));
		} catch (Exception e) {
			System.out.println("Test case 10: " + e.getMessage());
		}
	}

}

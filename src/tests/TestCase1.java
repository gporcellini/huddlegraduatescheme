package tests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import navigator.Nav;

public class TestCase1 {
	public Nav myNav;
	
	@Before
	public void initClass() {
		myNav = new Nav();
		myNav.addNodes(new String[] { "A", "B", "C", "D", "E" } );
		myNav.addPath("A", "B", 5);
		myNav.addPath("B", "C", 4);
		myNav.addPath("C", "D", 7);
		myNav.addPath("D", "C", 8);
		myNav.addPath("D", "E", 6);
		myNav.addPath("A", "D", 5);
		myNav.addPath("C", "E", 2);
		myNav.addPath("E", "B", 3);
		myNav.addPath("A", "E", 7);
	}

	@Test
	public void test01() {
		String result;
		try {
			result = "" + myNav.getRouteCost("A-B-C");
		} catch (Exception e) {
			result = e.getMessage().toUpperCase();
		}
		
		assertEquals("9", result);
	}
	
	@Test
	public void test02() {
		String result;
		try {
			result = "" + myNav.getRouteCost("A-D");
		} catch (Exception e) {
			result = e.getMessage().toUpperCase();
		}
		
		assertEquals("5", result);
	}
	
	@Test
	public void test03() {
		String result;
		try {
			result = "" + myNav.getRouteCost("A-D-C");
		} catch (Exception e) {
			result = e.getMessage().toUpperCase();
		}
		
		assertEquals("13", result);
	}
	
	@Test
	public void test04() {
		String result;
		try {
			result = "" + myNav.getRouteCost("A-E-B-C-D");
		} catch (Exception e) {
			result = e.getMessage().toUpperCase();
		}
		
		assertEquals("21", result);
	}
	
	@Test
	public void test05() {
		String result;
		try {
			result = "" + myNav.getRouteCost("A-E-D");
		} catch (Exception e) {
			result = e.getMessage().toUpperCase();
		}
		
		assertEquals("NO SUCH ROUTE", result);
	}

	@Test
	public void test06() {
		String result;
		try {
			result = "" + myNav.getRoutesWithNJunctions("C", "C", 3, true);
		} catch (Exception e) {
			result = e.getMessage().toUpperCase();
		}
		
		assertEquals("2", result);
	}
	
	@Test
	public void test07() {
		String result;
		try {
			result = "" + myNav.getRoutesWithNJunctions("A", "C", 4, false);
		} catch (Exception e) {
			result = e.getMessage().toUpperCase();
		}
		
		assertEquals("3", result);
	}
	
	@Test
	public void test08() {
		String result;
		try {
			result = "" + myNav.getShortestPathCost("A", "C");
		} catch (Exception e) {
			result = e.getMessage().toUpperCase();
		}
		
		assertEquals("9", result);
	}
	
	@Test
	public void test09() {
		String result;
		try {
			result = "" + myNav.getShortestPathCost("B", "B");
		} catch (Exception e) {
			result = e.getMessage().toUpperCase();
		}
		
		assertEquals("9", result);
	}
	
	@Test
	public void test10() {
		String result;
		try {
			result = "" + myNav.getLessThanXCostRoutes("C", "C", 30);
		} catch (Exception e) {
			result = e.getMessage().toUpperCase();
		}
		
		assertEquals("9", result);
	}
}
